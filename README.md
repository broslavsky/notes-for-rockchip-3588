# Notes for Rockchip 3588

This is a small summary regarding the Rockchip rk3588 SoC and the associated boards/support


## Mainline status

 * [Current kernel mainline support](/mainline-status.md)
 * [Current U-boot mainline support](/upstream_uboot.md)

## Board instructions

 * [Basic Radxa Rock-5B information](/rock5b-rk3588.md)
 * [Flashing the SPI flash memory on Rock-5B board](/flash_bootloader_spi.md)
 * [Build and Flash Debian](https://gitlab.collabora.com/hardware-enablement/rockchip-3588/debian-image-recipes)
 * [Automating Rock 5B maskrom boot mode](rock5b-maskrom-automation.md)
